# LLM - Detect AI Generated Text

https://www.kaggle.com/competitions/llm-detect-ai-generated-text

## Requirements

- `pytorch`
- `scikit-learn`
- `numpy`
- `pandas`
- `transformers`
- `datasets`
